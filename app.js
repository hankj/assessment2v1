//Load express
var express = require("express");
//Create an instance of express application
var app = express();

var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var q = require('q');

var mysql = require("mysql");
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "hankj",
    password: "philip01",
    database: "books",
    connectionLimit: 4
});


app.get("/api/books/:title/:author", function(req, res){
    console.log("Server search req params", req.params);

    pool.getConnection(function (err, connection) {
        if(err){
            res.status(500).end();
            return;
        }

        var finalSubSubString = "";
        var subStringTitle = "where title like " + "'%" + req.params.title + "%'";
        var subStringAuthorFirstName = "author_firstname like " + "'%" + req.params.author + "%'";
        var subStringAuthorLastName = "author_lastname like " + "'%" + req.params.author + "%'";
        
        //if both title and author search fields are empty
        if(req.params.title = ""){
            if(req.params.author == "")
                finalSubSubString ="limit 10"; //list all books without parameters but limit to 10
            else {
                finalSubSubString = "where" + subStringAuthorFirstName + " OR " + subStringAuthorLastName + " limit 10";
            }
        }
        else{ // title field not empty

            if(req.params.author == ""){  //title not empty and author field empty
                
                finalSubSubString = subStringTitle;  
            }
            else{    //title not empty and author field NOT empty
                
                finalSubSubString = subStringTitle + " AND " + subStringAuthorFirstName + " OR " + 
                    subStringAuthorLastName + " limit 10";
            }

        }

        console.log("Search String = %s", finalSubSubString);
        
        const FIND_ALL_BOOKS= "select id, author_lastname, author_firstname, title, cover_thumbnail from books ";

        connection.query(FIND_ALL_BOOKS+finalSubSubString,[],function(err, results){
            connection.release();
            if(err){
                console.info(err);
                res.status(500).end();
                return;
            }
            console.info(results);
            res.json(results);
        });
    })
});


app.post("/api/books/:bookId", function(req, res){
    console.log("Server edit req params", req.params);

    pool.getConnection(function (err, connection) {
        if(err){
            res.status(500).end();
            return;
        }

        const FIND_ONE_BOOK = "select id, author_lastname, author_firstname, title, cover_thumbnail from books where id = ?";

        connection.query(FIND_ONE_BOOK, [req.params.bookId],function(err, book){
            connection.release();
            if(err){
                console.info(err);
                res.status(500).end();
                return;
            }
            console.info("Server - Reply from DB: ",book[0]);
            res.json(book[0]);
        });
    });
});


app.post("/api/save", function(req, res){
    var book = JSON.parse(req.body.params.book);
    console.info("Server - Saving Book", book);

    pool.getConnection(function(err, connection){
        if(err){
            res.status(500).end();
            return;
        }
    
        const SAVE_BOOK_SQL = "UPDATE books SET title= ? , author_firstname= ? , author_lastname= ? WHERE id= ? ";

        connection.query(SAVE_BOOK_SQL,[book.title, book.author_firstname, book.author_lastname, book.id], function(err, result){
            connection.release();
            if(err){
                console.info(err);
                res.status(500).end();
                return;
            }
            console.info(result);
            res.status(202).json({url: "/updateCompleted"});
        });
    }); //getConnection
    
    
}); //post update


app.use(express.static(__dirname + "/public"));
// app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.listen(3000, function () {
    console.info("App Server started on port 3000");
});


